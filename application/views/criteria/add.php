<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Tambah Kriteria</h3>
            </div>
            <?php echo form_open('criteria/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="DueCount" class="control-label">Hari Keterlambatan</label>
						<a href="#" data-toggle="tooltip" title="Kol 2: s.d 60 hari, Kol 3 s.d 120 hari, Kol 4 s.d 180 hari, Kol 5 > 180 hari">Info</a>
						<div class="form-group">
							<input type="text" name="DueCount" value="<?php echo $this->input->post('DueCount'); ?>" class="form-control" id="DueCount" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="Paid" class="control-label">Lunas</label>
						<div class="form-group">
							<select name="Paid" id="Paid" class="form-control">
								<option value="1">Lunas</option>
								<option value="2">Menunggak Bunga</option>
								<option value="3">Menunggak Pokok</option>
								<option value="4">Menunggak Pokok + Bunga</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Action" class="control-label">Aksi</label>
						<div class="form-group">
							<select name="Action" id="Action" class="form-control">
								<option value="SMS">SMS</option>
								<option value="Call">Telepon</option>
								<option value="Email">Email</option>
								<option value="Survey">Kunjungan</option>
								<option value="SP">Surat Peringatan</option>
								<option value="Other">Lainnya</option>
							</select>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});
</script>