<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Kriteria Edit</h3>
            </div>
			<?php echo form_open('criteria/edit/'.$kriteria['ID']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="DueCount" class="control-label">DueCount</label>
						<div class="form-group">
							<input type="text" name="DueCount" value="<?php echo ($this->input->post('DueCount') ? $this->input->post('DueCount') : $kriteria['DueCount']); ?>" class="form-control" id="DueCount" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="Paid" class="control-label">Paid</label>
						<div class="form-group">
								<select name="Paid" id="Paid" class="form-control">
								<option value="1" <?php if ($kriteria['Paid'] == '1'){ echo 'selected';} ?>>Lunas</option>
								<option value="2" <?php if ($kriteria['Paid'] == '2'){ echo 'selected';} ?>>Menunggak Bunga</option>
								<option value="3" <?php if ($kriteria['Paid'] == '3'){ echo 'selected';} ?>>Menunggak Pokok</option>
								<option value="4" <?php if ($kriteria['Paid'] == '4'){ echo 'selected';} ?>>Menunggak Pokok + Bunga</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="Action" class="control-label">Action</label>
						<div class="form-group">
							<select name="Action" id="Action" class="form-control">
								<option value="SMS" <?php if ($kriteria['Action'] == 'SMS'){ echo 'selected';} ?>>SMS</option>
								<option value="Call" <?php if ($kriteria['Action'] == 'Call'){ echo 'selected';} ?>>Telepon</option>
								<option value="Email" <?php if ($kriteria['Action'] == 'Email'){ echo 'selected';} ?>>Email</option>
								<option value="Survey" <?php if ($kriteria['Action'] == 'Survey'){ echo 'selected';} ?>>Kunjungan</option>
								<option value="SP" <?php if ($kriteria['Action'] == 'SP'){ echo 'selected';} ?>>Surat Peringatan</option>
								<option value="Other" <?php if ($kriteria['Action'] == 'Other'){ echo 'selected';} ?>>Lainnya</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>