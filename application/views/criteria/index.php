<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Kriteria</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('criteria/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table id="table" class="table table-hover table-striped">
                    <thead>
                    <tr>
						<th>ID</th>
						<th>Hari Keterlambatan</th>
						<th>Tunggakan</th>
						<th>Aksi</th>
						<th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($kriteria as $C){ ?>
                    <tr>
						<td><?php echo $C['ID']; ?></td>
						<td><?php echo $C['DueCount']; ?></td>
                        <?php
                        if ($C['Paid'] == '1') {
                            echo '<td>Lunas</td>';
                        }

                        elseif ($C['Paid'] == '2') {
                            echo '<td>Tunggak Bunga</td>';
                        }

                        elseif ($C['Paid'] == '3') {
                            echo '<td>Tunggak Pokok</td>';
                        }

                        elseif ($C['Paid'] == '4') {
                            echo '<td>Tunggak Bunga + Pokok</td>';
                        }
                        ?>
						<td><?php echo $C['Action']; ?></td>
						<td>
                            <a href="<?php echo site_url('criteria/edit/'.$C['ID']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('criteria/remove/'.$C['ID']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>
