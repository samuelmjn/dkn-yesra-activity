<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Generate Data To-Do</h3>
            </div>
            <div class="box-body">
            	<h3>Disclaimer</h3>
                <p>
                    1. To-do hanya dapat digenerate <b>SATU</b> kali dalam sehari
                    <br> 2. Pengubahan kriteria hanya dapat berpengaruh pada to-do di hari selanjutnya
                    <br> 3. Membatalkan kegiatan hanya dapat dilakukan satu persatu
                </p>

                <br><a href="todo"><button class="btn btn-success">Generate To-do</button></a>
            </div>
        </div>
    </div>
</div>
