<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Import Data Pinjaman</h3>
            </div>
            <div class="box-body">
            	<form enctype="multipart/form-data" method="POST" action="<?php echo base_url(); ?>loan/upload">
                <label for="file">Upload File Pinjaman Bulan Ini : </label>
				<input type="file" name="file" id="file" size="20" class="form-control" required>
				<br>
				<button class="btn btn-primary" type="submit">Kegiatan Selesai</button>
                </form>              
            </div>
        </div>
    </div>
</div>
