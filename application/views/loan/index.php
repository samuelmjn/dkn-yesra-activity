<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Pinjaman Listing</h3>
            </div>
            <div class="box-body">
                <table id="table" class="table table-striped table-hover" style="width:100%">
                    <thead>
                    <tr>
						<th>ID</th>
						<th>ID Pinj</th>
						<th>CIF</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>No Telp</th>
						<th>No HP</th>
						<th>Email</th>
						<th>Pokok</th>
						<th>Bunga</th>
						<th>Denda</th>
						<th>Jth Tempo</th>
						<th>Petugas</th>
						<th>Actions</th>
                    </tr>
                	</thead>
                	<tbody>
                    <?php foreach($pinjaman as $L){ ?>
                    <tr>
						<td><?php echo $L['ID']; ?></td>
						<td><?php echo $L['LoanID']; ?></td>
						<td><?php echo $L['CIF']; ?></td>
						<td><?php echo $L['Name']; ?></td>
						<td><?php echo $L['Address']; ?></td>
						<td><?php echo $L['Phone']; ?></td>
						<td><?php echo $L['Mobile']; ?></td>
						<td><?php echo $L['Email']; ?></td>
						<td><?php echo $L['Principal']; ?></td>
						<td><?php echo $L['Interest']; ?></td>
						<td><?php echo $L['Fine']; ?></td>
						<td><?php echo $L['DueDate']; ?></td>
						<td><?php echo $L['OfficerID']; ?></td>
						<td>
                            <a href="<?php echo site_url('loan/edit/'.$L['ID']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('loan/remove/'.$L['ID']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>
